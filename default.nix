# Nix package for GNUnet development
#
## INSTALL
#
# To build and install the package in the user environment, use:
#
# $ nix-env -f . -i
#
## BUILD ONLY
#
# To build the package and add it to the nix store, use:
#
# $ nix-build
#
## SHELL
#
# To launch a shell with all dependencies installed in the environment, use one of the following:
#    $ nix-shell
#
# After entering nix-shell, build it:
#
# $ configurePhase
# $ buildPhase
#
## NIXPKGS
#
# For all of the above commands, nixpkgs to use can be set the following way:
#
# a) by default it uses nixpkgs pinned to a known working version
#
# b) use nixpkgs from the system:
#    --arg pkgs 0
#
# c) use nixpkgs at a given path
#    --arg pkgs /path/to/nixpkgs
#
## CCACHE
#
# To enable ccache, use the following:
#
#    --argstr ccache_dir /var/cache/ccache

# or when using nix-shell:
#    --argstr ccache_dir ~/.ccache
#
# and make sure the given directory is writable by the nixpkgs group when using nix-build or nix-env -i,
# or the current user when using nix-shell
#

{
 pkgs ? null,
 ccache_dir ? "",
}:

let
  syspkgs = import <nixpkgs> { };
  pinpkgs = syspkgs.fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs";

    # binary cache exists for revisions in https://nixos.org/releases/nixos/<release>/<build>/git-revision
    rev = "395a543f3605ea7c17797ad33fda0c251b802978"; # https://releases.nixos.org/nixos/18.09/nixos-18.09.2436.395a543f360/git-revision
    sha256 = "0az7333nr9fax6885kj7s61c0hs6wblj7a2y78k4pq0jnhjxqzzg";
  };
  usepkgs = if null == pkgs then
             import pinpkgs {}
            else
              if 0 == pkgs then
                import <nixpkgs> { }
              else
                import pkgs {};
  stdenv = usepkgs.stdenvAdapters.keepDebugInfo usepkgs.stdenv;

in {
  gnunet-dev = usepkgs.callPackage ./gnunet-dev.nix {
    inherit ccache_dir;
  };
}
